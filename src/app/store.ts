import { configureStore } from "@reduxjs/toolkit";
import CounterReducer from "features/counteSlice";
import TodoReducer from "features/todosSlice";

export const store = configureStore({
	reducer: {
		counter: CounterReducer,
		todo: TodoReducer,
	},
	devTools: !import.meta.env.PROD
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;