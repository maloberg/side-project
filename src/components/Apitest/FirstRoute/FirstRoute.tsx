import axios from "axios";
import { useQuery } from "react-query";
import FormPost from "./FormPost/FormPost";
import Post from "./Post/Post";
import { PostI } from "./Post/Post.interface";

import styles from "./FirstRoute.module.scss";

export const getPost = async () => (
	(await axios.get('https://jsonplaceholder.typicode.com/posts')).data
);

function FirstRoute() {
	const { data, error, isLoading } = useQuery<PostI[], Error>("jose", getPost);

	if (isLoading) {
		return <div>Loading ...</div>;
	}

	if (error) {
		return <div>une erreur est survenue</div>;
	}

	return (
		<>
			<div className={ styles.posts }>
				<FormPost />
				<div className={ styles.posts__container }>
					{ data?.map(post => (
						<Post key={ post.id } { ...post } />
					)) }
				</div>
			</div>
		</>
	);
}

export default FirstRoute;