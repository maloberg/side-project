import axios from "axios";
import { FormEvent, useState } from "react";
import { useMutation, useQueryClient } from "react-query";

import styles from "./FormPost.module.scss";

type submitPost = {
	title: string;
	userId?: string;
	body: string;
};

const postPost = ({ title, body, userId = "1" }: submitPost) => (
	axios.post("https://jsonplaceholder.typicode.com/posts", { title, body, userId })
);

function FormPost() {
	const queryClient = useQueryClient();
	const [ error, setError ] = useState("");

	const mutation = useMutation(postPost, {
		onSuccess: () => { queryClient.invalidateQueries("jose"); }
	});

	const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		if (error) {
			setError("");
		}

		const target = e.target as HTMLFormElement;
		const formData = new FormData(target);

		const title = String(formData.get("title"));
		const body = String(formData.get("body"));

		if (!title) {
			setError("Veuillez ajouter un titre");
			return;
		}

		mutation.mutate({ title, body });
		target.reset();
	};

	return (
		<form onSubmit={ handleSubmit } className={ styles.form }>
			<div style={ { marginBottom: "10px" } }>
				<label className={ styles.label } htmlFor="title">title</label>
				<input className={ styles.input } type="text" id="title" />
			</div>
			<div>
				<label className={ styles.label } htmlFor="body">body</label>
				<textarea className={ styles.input } id="body" name="body" />
			</div>
			<button className={ styles.submit } type="submit"> submit </button>
		</form>
	);
}

export default FormPost;