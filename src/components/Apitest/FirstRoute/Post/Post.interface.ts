
export type PostI = {
	userId: string;
	id: number;
	title: string;
	body: string;
};