import { memo } from 'react';
import { useNavigate } from "react-router";
import { PostI } from "../Post/Post.interface";
import styles from "./Post.module.scss";

function Post({ id, title, body }: PostI) {
	const navigation = useNavigate();

	return (
		<div className={ styles.post } onClick={ () => navigation(`${id}`) } key={ id } >
			<h2>{ title }</h2>
			<div>
				<p>{ body }</p>
			</div>
		</div >
	);
}

export default memo(Post);