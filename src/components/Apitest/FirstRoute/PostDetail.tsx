import axios from "axios";
import { useQuery, useQueryClient } from "react-query";
import { useParams } from "react-router";
import { PostI } from "./Post/Post.interface";
import Post from "./Post/Post";

const getPostDetail = async (id: string) => (
	(await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)).data as PostI
);


function PostDetail() {
	const queryClient = useQueryClient();
	const { id } = useParams();
	const { data, isLoading, error } = useQuery<PostI, Error>([ "jose", id ], () => getPostDetail(String(id)), {
		initialData: () => (
			(queryClient.getQueryData("jose") as PostI[])?.find(d => String(d.id) === id)
		),

		initialDataUpdatedAt: () => (
			queryClient.getQueryState('todos')?.dataUpdatedAt
		),
	});

	if (isLoading) {
		return (<div>isLoading</div>);
	}

	return (
		<div style={ { marginTop: "200px" } }>
			<Post { ...data as PostI } />
		</div>
	);
}

export default PostDetail;