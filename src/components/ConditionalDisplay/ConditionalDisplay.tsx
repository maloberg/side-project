import { ReactNode } from 'react';

interface Props {
	condition: boolean;
	children: ReactNode;
	fallback?: ReactNode;
}

function ConditionalDisplay({ condition, children, fallback }: Props) {
	const display = condition
		? children
		: fallback
			? fallback
			: <></>;

	return (<>{ display }</>);
}

export default ConditionalDisplay;