import { amountAdded, decrement, getCountFromStore, increment, reset } from 'features/counteSlice'
import { useAppDispatch } from 'hooks/hooks';
import { useState } from 'react';
import { useSelector } from 'react-redux'

function Counter() {
	const count = useSelector(getCountFromStore);
	const [valueToAdd, setValueToAdd] = useState(0);
	const dispatch = useAppDispatch();	

	return (
		<>
			<div>{ count }</div>
			<div onClick={() => dispatch(increment())}>add</div>
			<div onClick={() => dispatch(decrement())}>decrement</div>
			<label htmlFor="">set The value to add</label>
			<input type="text" value={ valueToAdd } onKeyUp={(e) => e.key === "Enter"? dispatch(amountAdded(valueToAdd)):""} onChange={ e => setValueToAdd(Number(e.target.value)|| 0) } />
			<div onClick={() => dispatch(amountAdded(valueToAdd))}>ValueToAdd</div>
			<div onClick={() => dispatch(reset())}>reset</div>
			<div>Counter</div>
		</>
	)
}

export default Counter