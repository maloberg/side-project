import { memo } from "react";

import { remove, Todo as TodoType, toggleDone } from "features/todosSlice";
import { useAppDispatch } from "hooks/hooks";
import { DragTodo } from '../TodoList/TodoList';

function Todo({ done, id, title }: TodoType) {
	const dispatch = useAppDispatch();

	const handleDone = () => { dispatch(toggleDone(id)); };

	const handleDelete = () => { dispatch(remove(id)); };

	const handleDrag = (e: DragTodo) => {
		const element = e.target as HTMLElement;

		element.style.background = "red";

		e.dataTransfer.setData("text", JSON.stringify({
			todoList: "test",
			todo: {
				done,
				title,
				id
			}
		}));
	};

	const handleDragEnd = (e: DragTodo) => {
		const element = e.target as HTMLDivElement;
		element.style.background = "green";
	};

	return (
		<div className="fade-in" draggable onDragStart={ handleDrag } onDragEnd={ handleDragEnd } >
			<input type="checkbox" onChange={ handleDone } checked={ done } />
			<div>{ title }</div>
			<button onClick={ handleDelete }>x</button>
		</div>
	);
}

export default memo(Todo); 