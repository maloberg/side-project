import { DragEvent, KeyboardEvent } from "react";

import { add, clear, getTodosFromStore } from "features/todosSlice";
import { useAppDispatch } from "hooks/hooks";
import { useSelector } from "react-redux";
import List from "components/List/List";
import Todo from "../Todo/Todo";
import style from "./TodoList.module.scss";

export type DragTodo = DragEvent<HTMLDivElement>;

function TodoList() {
	const todos = useSelector(getTodosFromStore);
	const dispatch = useAppDispatch();


	const handleSubmit = (target: EventTarget) => {
		const input = target as HTMLInputElement;
		const title = input.value;
		input.value = '';

		if (title) { dispatch(add({ title })); };
	};

	const allowDrop = (e: DragTodo) => {
		e.preventDefault();
	};

	const handleDrop = (e: DragTodo) => {
		console.log(JSON.parse(e.dataTransfer.getData("text")));
		console.log(e.currentTarget);
	};

	const handleClear = () => { dispatch(clear()); };

	const handleKeyUp = (e: KeyboardEvent<HTMLInputElement>) => {
		if (e.key === "Enter") { handleSubmit(e.target); };
	};

	return (
		<div className={ style[ "todo-container" ] }>
			<input type="text"
				name="title"
				onKeyUp={ handleKeyUp }
			/>
			<div onClick={ handleClear }>Delete all</div>
			<div onDrop={ handleDrop } onDragOver={ allowDrop }>
				<List items={ todos }>
					{ item => (<Todo { ...item } />) }
				</List>
			</div>
		</div>
	);
}

export default TodoList;