import { menu } from "components/Header/MenuList";
import List from "components/List/List";
import { Link } from "react-router-dom";
import style from './Navigation.module.scss';
interface Props {
	open: boolean;
	toggleOpen: () => void;
}

const Navigation = ({ open, toggleOpen }: Props) => (
	<nav className={ `${style.navigation} ${open && style[ "navigation--open" ]}` }>
		<List items={ menu }>
			{ ({ label, path }) => (
				<Link
					onClick={ toggleOpen }
					to={ path }
				>
					{ label }
				</Link>
			) }
		</List>
	</nav>
);

export default Navigation;