import { ReactNode } from 'react';
interface Props<Item> {
	items: Item[],
	children: (item: Item, index: number) => ReactNode;
}

function List<Item>({ items, children }: Props<Item>) {
	return (
		<ul>
			{ items.map((item, index) => (
				// @ts-ignore:next-line
				<li key={ item.id }>{ children(item, index) }</li>
			)) }
		</ul>
	);
}

export default List;
