import { FormEvent, useCallback } from 'react';
import { MailingData } from './interfaces/MailingData';
import axios from 'axios';

function Mailing() {
	const handleSubmit = useCallback(async (event: FormEvent<HTMLFormElement>) => {
		// 	event.preventDefault()
		// 	const formData = new FormData(event.target as HTMLFormElement);
		// 	const { email, message } = (Object.fromEntries(formData.entries()) as unknown) as MailingData;

		// 	const res = await axios.post("https://formsubmit.co/ajax/malo.bergade@gmail.com", {
		// 		message,
		// 		email
		// 	});
	}, []);

	return (
		<form onSubmit={ handleSubmit }>
			<label htmlFor="">name</label>
			<input id="name" type="text" name="name" />
			<label htmlFor="email">email</label>
			<input id="email" type="text" name="email" />
			<label htmlFor="message">message</label>
			<textarea id="message" name="message" />
			<button type='submit'></button>
		</form>
	);
}

export default Mailing;