export interface MailingData {
	message: string,
	email: string
}