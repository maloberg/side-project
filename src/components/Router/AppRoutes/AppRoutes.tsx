import { Route, Routes } from "react-router-dom";
import React, { Suspense } from "react";

const ReduxContext = React.lazy(() => import("pages/ReduxContext"));
const Counter = React.lazy(() => import("components/Counter/Counter"));
const TodoList = React.lazy(() => import("components/Counter/TodoList/TodoList"));
const Apitest = React.lazy(() => import("components/Apitest/Apitest"));
const Layout = React.lazy(() => import("pages/Layout"));
const Home = React.lazy(() => import("pages/Home"));
const Context = React.lazy(() => import("pages/Context"));
const PostDetail = React.lazy(() => import("components/Apitest/FirstRoute/PostDetail"));

const AppRoutes = () => (
	<Suspense fallback={ <div>Chargement...</div> }>
		<Routes>
			<Route path="/" element={ <Layout /> }>
				<Route index element={ <Home /> } />
				<Route path="bob" element={ <ReduxContext /> } >
					<Route index element={ <Counter /> } />
					<Route path="todos" element={ <TodoList /> } />
				</Route>
				<Route path="query" element={ <Apitest /> } />
				<Route path="query/:id" element={ <PostDetail /> } />
				<Route path="/context" element={ <Context /> } />
				<Route path='*' element={ <div>404</div> } />
			</Route>
		</Routes>
	</Suspense>
);

export default AppRoutes;