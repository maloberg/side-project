import { BrowserRouter } from 'react-router-dom'
import AppRoutes from './AppRoutes/AppRoutes'

function Router() {
	return (
		<BrowserRouter>
			<AppRoutes />
		</BrowserRouter>
	)
}

export default Router
