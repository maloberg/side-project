import { Role, useAuth } from "context/AuthContext";
import { FormEvent } from "react";

function Login() {
	const { login } = useAuth();
	const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		const formData = new FormData(e.target as HTMLFormElement);
		login({
			role: Role.User,
			token: "dabudidabuda",
			username: formData.get("username") as string
		});
	};


	return (
		<form onSubmit={ handleSubmit }>
			<label>Username</label>
			<input type="text" name="username" />
			<label>Password</label>
			<input type="password" name="password" />
			<button type="submit">Submit</button>
		</form>
	);
}

export default Login;