import { useAuth } from "context/AuthContext";

function Logout() {
	const { logout, user } = useAuth();

	return (
		<>
			{ user ? <div onClick={ logout }>Deconnection</div> : null }
		</>
	);
}

export default Logout;