import { useAuth } from "context/AuthContext";
import Logout from "./Logout/Logout";
import Login from "./Login/Login";

function TestProvider() {
	const { user } = useAuth();
	return (
		<>
			<div>{ JSON.stringify(user) }</div>
			<Login />
			<Logout />
		</>
	);
}

export default TestProvider;
