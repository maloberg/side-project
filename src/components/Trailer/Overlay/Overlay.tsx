import { ChangeEvent } from 'react';
import styles from './Overlay.module.scss';

interface Props {
	toggleMute: () => void;
	soundLevel: (number: number) => void;
	handleFullScreen: () => void;
	display: boolean;
}

function Overlay({
	toggleMute,
	soundLevel,
	handleFullScreen,
	display
}: Props) {

	const handleSoundChange = (e: ChangeEvent<HTMLInputElement>) => {
		soundLevel(Number(e.target.value) / 100);
	};

	return (
		<div className={ `${styles.overlay} ${display && styles[ "overlay--display" ]}` }>
			<input type="range" className={ styles.overlay__progress } />

			<div className={ styles.overlay__settings }>
				<div onClick={ toggleMute } className='trailer__mute'>mute</div>
				<div className='trailer__volume'>volume</div>
				<input type="range" onChange={ handleSoundChange } />
				<div onClick={ handleFullScreen }>fullscreen</div>
			</div>
		</div>
	);
}

export default Overlay;