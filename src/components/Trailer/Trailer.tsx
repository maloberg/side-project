import video from "assets/20220112_213502.mp4";
import useVideo from "hooks/useVideo";
import styles from "./Trailer.module.scss";
import Overlay from "./Overlay/Overlay";

function Trailer() {
    const {
        mute,
        paused,
        display,
        videoRef,
        toggleMute,
        soundLevel,
        handleFullScreen,

    } = useVideo();

    return (
        <div className={ styles.trailer }>
            <video
                className={ styles.trailer__screen }
                loop
                muted={ mute }
                src={ video }
                ref={ videoRef }
            />
            <Overlay
                toggleMute={ toggleMute }
                soundLevel={ soundLevel }
                handleFullScreen={ handleFullScreen }
                display={ paused || display }
            />
        </div>
    );
}

export default Trailer;
