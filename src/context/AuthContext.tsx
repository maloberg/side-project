import { createContext, ReactNode, useContext, useEffect, useState } from "react";

export enum Role {
	SuperAdmin,
	Admin,
	User
}

type User = {
	username: string,
	token: string;
	role: Role;
};

type AuthContextState = {
	login: ({ }: User) => void;
	logout: () => void;
	user: User | null;
};

const AuthContext = createContext({} as AuthContextState);

type Props = {
	children: ReactNode;
};

export const AuthContextProvider = ({ children }: Props) => {
	const [ user, setUser ] = useState(null as User | null);

	const login = (user: User) => {
		localStorage.setItem("user", JSON.stringify(user));
		setUser(user);
	};

	const logout = () => { setUser(null); };

	useEffect(() => {
		let json = localStorage.getItem("user");

		if (!json) {
			return;
		}

		const user = JSON.parse(json) as User;

		setUser(user);

	}, []);

	const value = {
		login,
		logout,
		user,
	};

	return (
		<AuthContext.Provider value={ value }>
			{ children }
		</AuthContext.Provider>
	);
};

export const useAuth = () => useContext(AuthContext);
