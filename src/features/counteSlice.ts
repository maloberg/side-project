import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "app/store";

interface CounterState {
	count: number;
}

const initialState: CounterState = {
	count: 0,
};

const CounterSlice = createSlice({
	name: "counter",
	initialState,

	reducers: {
		increment: (state) => {
			state.count++;
		},

		decrement: (state) => {
			if (state.count > 0) {
				state.count--;
			}
		},

		reset: (state) => {
			state.count = 0;
		},

		amountAdded: (state, action: PayloadAction<number>) => {
			state.count += action.payload;
		}
	}
});

export const { increment, decrement, reset, amountAdded } = CounterSlice.actions;
export const getCountFromStore = (state: RootState) => state.counter.count;

export default CounterSlice.reducer;