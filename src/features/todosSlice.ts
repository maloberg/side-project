import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/store';

export interface Todo {
	id: string;
	title: string;
	done: boolean;
}

interface TodosState {
	todos: Todo[];
}

type payloadTodo = Omit<Todo, "id" | "done">;

const todosFromStorage = localStorage.getItem("todos");

let todos: Todo[] = todosFromStorage
	? JSON.parse(todosFromStorage) as Todo[]
	: [];

const initialState: TodosState = { todos };

const todosSlice = createSlice({
	name: "todos",
	initialState,
	reducers: {
		add: (state, action: PayloadAction<payloadTodo>) => {
			state.todos.push({
				id: `${Math.random() * Math.random()}bobi${action.payload.title.slice(0, Math.random() * 10)}`,
				done: false,
				...action.payload
			});

			localStorage.setItem("todos", JSON.stringify(state.todos));
		},

		toggleDone: (state, action: PayloadAction<string>) => {
			state.todos.map(todo => {
				if (todo.id === action.payload) {
					todo.done = !todo.done;
					localStorage.setItem("todos", JSON.stringify(state.todos));
				}
			});
		},

		remove: (state, action: PayloadAction<string>) => {
			state.todos = state.todos.filter(todo => todo.id !== action.payload);
			localStorage.setItem("todos", JSON.stringify(state.todos));
		},

		clear: (state) => {
			state.todos = [];
			localStorage.removeItem("todos");
		}
	}
});

export const { add, toggleDone, remove, clear } = todosSlice.actions;
export const getTodosFromStore = (state: RootState) => state.todo.todos;

export default todosSlice.reducer;