import { useState, useRef, useEffect, useCallback } from "react"
import { debounce } from "tools/debounce";

function useVideo() {
	const [mute, setMute] = useState(false);
	const [paused, setPaused] = useState(true);
	const [display, setDisplay] = useState(false);

	const videoRef = useRef<HTMLVideoElement>(null);

	useEffect(() => {
		const { current } = videoRef;

		if (!current?.parentElement)  { return };
		
		const container = current.parentElement;
		
		window.addEventListener('keyup', handleKeyUp);
		container.addEventListener("mouseleave", setDisplayFalse);
		container.addEventListener("mouseenter", setDisplayTrue);
		container.addEventListener("dblclick", handleFullScreen);
		container.addEventListener("mousemove", debounce(handleMouseMove, 700, beforeDebounce));
		current.addEventListener("click", handlePlaying);

		return () => {
			window.removeEventListener('keyup', handleKeyUp);
			container.removeEventListener("mousemove", debounce(handleMouseMove, 700) );
			container.removeEventListener("mouseenter", setDisplayTrue);
			container.removeEventListener("mouseleave", setDisplayFalse);
			container.removeEventListener("dblclick", handleFullScreen);
			current.removeEventListener("click", handlePlaying);
		}
	}, []);

	const beforeDebounce = () => {
		if(display) { return };

		const { current } = videoRef;

		if(!current) { return };

		setDisplayTrue();
		current.classList.remove('cursorHide');
	}
	
	const handleMouseMove = useCallback( () => {
		const { current }  = videoRef

		if(!current) { return }; 

		current.classList.add('cursorHide');
		setDisplayFalse();
	},[])

	const handleKeyUp = (e: KeyboardEvent) => {
		if(e.key === ' ' || e.key === 'k')  { handlePlaying() };
		if(e.key === 'm') { toggleMute() };
		if(e.key === 'f') { handleFullScreen() };
	};

	const setDisplayTrue = () => { setDisplay(true) };

	const setDisplayFalse = () => { setDisplay(false) };

	const handleFullScreen = async () => {
		const { current } = videoRef;

		if(!current) return;

		if(!document.fullscreenElement && current.parentElement) {
			try {
				await current.parentElement.requestFullscreen();
			} catch(error) {
				console.error('error', error);
			}
			return;
		}

		await document.exitFullscreen();
	}

	/* ================================== */
	/*             PLAY/PAUSE             */
	/* ================================== */

	const play = async (currentVideoRef: HTMLVideoElement) => {
		setPaused(false);
		await currentVideoRef.play();
	};

	const pause = (currentVideoRef: HTMLVideoElement) => {
		setPaused(true);
		currentVideoRef.pause();
	};

	const handlePlaying = async () => {
		const { current } = videoRef;

		if(!current) { return };
		current.paused ? await play(current) : pause(current);
	};

	/* ================================== */
	/*               VOLUME               */
	/* ================================== */

	const toggleMute = () => { setMute( prev => !prev ) };

	const soundLevel = (percentage: number) => {
		const { current } = videoRef;
		if(!current) { return };

		current.volume = percentage;
	}

	return { 
		mute, 
		videoRef, 
		paused, 
		display,
		toggleMute, 
		soundLevel,
		handleFullScreen,
	}
}

export default useVideo 