import React from "react";
import ReactDOM from 'react-dom/client';

import Router from 'components/Router/Router';
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import 'scss/reset.scss';

const client = new QueryClient({
	defaultOptions: {
		queries: {
			staleTime: 50000
		}
	}
});

const App = () => (
	<QueryClientProvider client={ client }>
		<React.StrictMode>
			<Router />
		</React.StrictMode>
		{ import.meta.env.DEV ? <ReactQueryDevtools /> : null }
	</QueryClientProvider>
);


const container = document.getElementById('root')!;
const root = ReactDOM.createRoot(container);
root.render(<App />);
