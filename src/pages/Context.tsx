import TestProvider from "components/TestProvider/TestProvider";
import { AuthContextProvider } from "context/AuthContext";

function Context() {
	return (
		<AuthContextProvider>
			<TestProvider />
		</AuthContextProvider>
	);
}

export default Context;
