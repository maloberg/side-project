import Header from "components/Header/Header";
import { Outlet } from "react-router-dom";

function Layout() {
	return (
		<>
			<Header />
			<div className="scene container">
				<Outlet />
			</div>
		</>
	);
}

export default Layout;