import { store } from "app/store";
import { Provider } from "react-redux";
import { Outlet } from "react-router";
import MenuApplications from "components/Counter/MenuApplication/MenuApplications";

const ReduxContext = () => (
	<Provider store={ store }>
		<MenuApplications />
		<Outlet />
	</Provider>
);

export default ReduxContext;
