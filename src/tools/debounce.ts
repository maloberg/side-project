export function debounce(fn: any, timeout = 300, beforeDebounce?: any) {
	let timer: any;
	return (...args: any) => {
		if (beforeDebounce) { beforeDebounce(); }

		clearTimeout(timer);
		timer = setTimeout(
			() => { fn.apply(args.container, ...args); }
			, timeout
		);
	};
}
